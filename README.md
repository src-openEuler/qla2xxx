# marvelldriver

## 介绍
provide Marvell QLogic FC HBA adapter driver for openEuler LTS version.

Ethernet NIC driver and SAS/SATA driver will provide in other repository.

## 软件架构
QLogic driver package for openEuler system. 

FC HBA driver is from [Marvell/QLogic webisite](https://www.marvell.com/support/downloads.html)



### FC HBA driver 
> qla2xxx-x.y.z




## 安装教程

### QLogic FC HBA driver

1. 本仓库用于生成openEuler 版本的KMOD 驱动RPM 安装包。
2. 支持 openEuler 20.03 LTS SP3 及 openEuler 22.03 LTS 版本。
3. 支持 ARM64 及 X64 平台。
4. 支持国产CPU。