%define dvendor         qlgc
%define vendor          %{dvendor}
%define driver_name     qla2xxx
%define dist_version    10.02.08.01-k
%define version         10.02.08.01
%define release	        1
%define BUILD_KERNEL_SPEC %( if [ -z "${BUILD_KERNEL}" ]; then BUILD_KERNEL=` rpm -q kernel-devel |sed 's/.*devel-//g' `; fi; echo "${BUILD_KERNEL}" | cut -d "-" -f -2 )

%define mod_sign_files /home/sign
%define module_key %{mod_sign_files}/qlgc_module_signing_key-202x.priv
%define module_cer %{mod_sign_files}/qlgc_module-202x.der

%define gpginfo "RPM public key available at http://ldriver.qlogic.com/RPM-public-key/ and Module public key available at http://ldriver.qlogic.com/Module-public-key/"

Name:           %{vendor}-%{driver_name}
Version:        %{version}
Release:        %{release}
Summary:        QLogic qlaxxx FC Driver Update Program package
Group:          System/Kernel
License:        GPLv2
Vendor:         QLogic Corp.
Packager:       QLogic Corp.

URL:            http://www.qlogic.com 

Source0:        %{driver_name}-%{dist_version}.tar.gz
Source1:        kmodtool-nvme.8.x.sh
Source2:        dracut.qla2xxx.conf
Source3:        99-qla2xxx.rules
Source4:        qla2xxx_udev.sh


BuildRoot:      %(mktemp -ud %{_tmppath}/%{name}-src-%{dist_version}-%{release}-XXXXXX)
BuildRequires:  kernel-rpm-macros
BuildRequires:  kernel-devel

BuildRequires:  hostname 
BuildRequires:  procps-ng
BuildRequires:  systemd-udev



%global debug_package %{nil}

%description
OE Driver Update package for QLogic FC Adapter.
Public key available at http://ldriver.qlogic.com/RPM-public-key/
SRPMS available at http://ldriver.qlogic.com/driver-srpms/


%prep
%setup -n %{driver_name}-%{dist_version}
set -- *
mkdir source
mv "$@" source/
mkdir obj
echo "override %{driver_name} * weak-updates/%{dvendor}-%{driver_name}" > %{driver_name}.conf

%build
for flavor in 'default'; do
rm -rf obj/$flavor
cp -r source obj/$flavor
cd obj/$flavor
make -C /lib/modules/` rpm -q kernel-devel |sed 's/.*devel-//g' `/build M=`pwd` modules

done

%install
export LD_LIBRARY_PATH=/usr/lib/
export INSTALL_MOD_PATH=$RPM_BUILD_ROOT
export INSTALL_MOD_DIR=extra/%{name}
for flavor in %flavors_to_build ; do
if [ -f %{module_key} ] && [ -f %{module_cer} ]; then
/usr/src/kernels/` rpm -q kernel-devel |sed 's/.*devel-//g' `/scripts/sign-file sha256 %{module_key} %{module_cer}  $PWD/obj/$flavor/%{driver_name}.ko
fi


dest_dir=${RPM_BUILD_ROOT}/lib/modules/` rpm -q kernel-devel |sed 's/.*devel-//g' `/extra/%{name}/

mkdir -p $dest_dir

install -m 444 obj/default/qla2xxx.ko $dest_dir


# Cleanup unnecessary kernel-generated module dependency files.
find $INSTALL_MOD_PATH/lib/modules -iname 'modules.*' -exec rm {} \;
done


%{__install} -d $RPM_BUILD_ROOT%{_sysconfdir}/depmod.d/
%{__install} %{driver_name}.conf $RPM_BUILD_ROOT%{_sysconfdir}/depmod.d/
mkdir -p $RPM_BUILD_ROOT/etc/dracut.conf.d
%{__install} -m 755 %{S:2} $RPM_BUILD_ROOT/etc/dracut.conf.d/qla2xxx.conf
mkdir -p $RPM_BUILD_ROOT/etc/udev/rules.d/
%{__install} -m 644 %{S:3} $RPM_BUILD_ROOT/etc/udev/rules.d/

mkdir -p $RPM_BUILD_ROOT/lib/udev
%{__install} -m 744 %{S:4} $RPM_BUILD_ROOT/lib/udev/
#systemctl enable qla2xxx-nvmefc-boot-connection.service

# which udevadm 1>/dev/null 2>&1
# if [ $? -eq 0 ]; then
# udevadm control -R 
# else
# udevcontrol reload_rules
# fi


%post
depmod
dracut -f
#systemctl enable qla2xxx-nvmefc-boot-connection.service

%postun
depmod
dracut -f




%files
%defattr(644,root,root,755)
/lib/modules/
/etc/depmod.d/qla2xxx.conf
/etc/dracut.conf.d/qla2xxx.conf
/etc/udev/rules.d/99-qla2xxx.rules 
/lib/udev/qla2xxx_udev.sh

%clean
rm -rf $RPM_BUILD_ROOT


%changelog

* Wed Nov 16 2022 QLogic Corp <support@qlogic.com> - 10.02.08.01-1 
- Changes: Add support for creating initrd image on openEuler, Kylin and UOS

* Tue Sep 27 2022 QLogic Corp <support@qlogic.com> - 10.02.08.00.a7-1
- package init
- DESC: 10.02.08.00.a7-k XL 09/27/2022. Fisrt version for OE community.


